﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using CarPark.Data;
using CarPark.Data.Models;
using CarPark.Fees;

namespace ConsoleInterface {
    public class Program {
        public static void Main(string[] args) {
            Console.WriteLine("-- Car Park Rate Calculation Engine --");
            Console.WriteLine("Press Enter to continue...");
            Console.ReadLine();

            while (true) {
                try {
                    ProcessInput();
                } catch (Exception ex) { // in general catching Exception is
                    // considered bad practice
                    Console.WriteLine("An error occurred: {0}", ex.Message);
                    Console.WriteLine();
                }
                Console.Write("Enter 'exit' to exit, or press Enter to " +
                              "perform a new calculation... ");

                if (Console.ReadLine().ToLower() == "exit")
                    break;
                Console.WriteLine();
            }
        }

        private static void ProcessInput() {
            DateTime entry, exit;

            entry = ReadDateTime("Enter an entry date and time " +
                                 "(dd/mm/yyyy hh:mm):");
            exit = ReadDateTime("Enter an exit date and time " +
                                "(dd/mm/yyyy hh:mm):");

            var feeCalculatorFactory = new FeeCalculatorFactory(
                new HardCodedConstantRepository());
            var feeCalculator = feeCalculatorFactory.NewFeeCalculator(
                new ParkingRecord() {
                    EntryDateTime = entry,
                    ExitDateTime = exit
                }
            );

            double fee = feeCalculator.Fee();

            Console.WriteLine();
            Console.WriteLine("Rate: {0}", feeCalculator.CanonicalName);
            Console.WriteLine("Fee: {0}", fee);
            Console.WriteLine();
        }

        private static DateTime ReadDateTime(string message) {
            while (true) {
                Console.Write(message.Trim() + " ");
                string input = Console.ReadLine();
                DateTime dateTime;
                try {
                    dateTime = DateTime.Parse(input, new CultureInfo("en-AU"));
                } catch (FormatException) {
                    Console.WriteLine("Invalid format");
                    continue;
                }
                return dateTime;
            }
        }

    }
}