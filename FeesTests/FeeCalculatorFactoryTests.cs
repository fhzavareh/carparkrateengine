﻿using System;
using System.Collections.Generic;
using System.Text;
using CarPark.Data;
using CarPark.Data.Models;
using CarPark.Fees;
using Xunit;
// ReSharper disable PossibleInvalidOperationException

namespace CarPark.Tests.FeesTests {

    public class FeeCalculatorFactoryTests {
        private const string EbFromLowKey =
          "CarPark.Fees.FeeCalculatorFactory.EarlyBirdFromLow";
        private const string EbFromHighKey =
            "CarPark.Fees.FeeCalculatorFactory.EarlyBirdFromHigh";
        private const string EbToLowKey =
            "CarPark.Fees.FeeCalculatorFactory.EarlyBirdToLow";
        private const string EbToHighKey =
            "CarPark.Fees.FeeCalculatorFactory.EarlyBirdToHigh";
        private const string NightFromLowKey =
            "CarPark.Fees.FeeCalculatorFactory.NightFromLow";
        private const string NightFromHighKey =
            "CarPark.Fees.FeeCalculatorFactory.NightFromHigh";
        private const string NigthToKey =
            "CarPark.Fees.FeeCalculatorFactory.NightTo";

        private readonly double ebFromLow, ebFromHigh, ebToLow,
            ebToHigh, nightFromLow, nightFromHigh, nightTo;

        private readonly ConstantRepository constantRepository;

        public FeeCalculatorFactoryTests() {
            this.constantRepository = new HardCodedConstantRepository();

            this.ebFromLow = this.constantRepository.TryGetValue<double?>(
                EbFromLowKey).Value;
            this.ebFromHigh = this.constantRepository.TryGetValue<double?>(
                EbFromHighKey).Value;
            this.ebToLow = this.constantRepository.TryGetValue<double?>(
                EbToLowKey).Value;
            this.ebToHigh = this.constantRepository.TryGetValue<double?>(
                EbToHighKey).Value;
            this.nightFromLow = this.constantRepository.TryGetValue<double?>(
                NightFromLowKey).Value;
            this.nightFromHigh = this.constantRepository.TryGetValue<double?>(
                NightFromHighKey).Value;
            this.nightTo = this.constantRepository.TryGetValue<double?>(
                NigthToKey).Value;
        }

        [Fact]
        public void NewFeeCalculator_ReturnsWeekend_WhenBorder() {
            var parkingRecord = new ParkingRecord() {
                // Sat 00:00 to Mon 00:00
                EntryDateTime = new DateTime(2017, 5, 20, 0, 0, 0),
                ExitDateTime = new DateTime(2017, 5, 21, 23, 59, 0)
            };
            var factory = new FeeCalculatorFactory(this.constantRepository);
            var instance = factory.NewFeeCalculator(parkingRecord);

            Assert.IsType<WeekendFeeCalculator>(instance);
        }

        [Fact]
        public void NewFeeCalculator_ReturnsWeekend() {
            var parkingRecord = new ParkingRecord() {
                // Sat 15:20 to Sun 11:00
                EntryDateTime = new DateTime(2017, 5, 20, 15, 20, 0),
                ExitDateTime = new DateTime(2017, 5, 21, 11, 0, 0)
            };
            var factory = new FeeCalculatorFactory(this.constantRepository);
            var instance = factory.NewFeeCalculator(parkingRecord);

            Assert.IsType<WeekendFeeCalculator>(instance);
        }

        [Fact]
        public void NewFeeCalculator_ReturnsNight_WhenBorder1()
        {
            var parkingRecord = new ParkingRecord() {
                EntryDateTime = (new DateTime(2017, 5, 22))
                                .AddHours(this.nightFromLow),
                ExitDateTime = (new DateTime(2017, 5, 22))
                                .AddHours(this.nightTo).AddMinutes(-1)
            };
            var factory = new FeeCalculatorFactory(this.constantRepository);
            var instance = factory.NewFeeCalculator(parkingRecord);

            Assert.IsType<NightFeeCalculator>(instance);
        }

        [Fact]
        public void NewFeeCalculator_ReturnsNight_WhenBorder2() {
            var parkingRecord = new ParkingRecord() {
                // Sat 15:20 to Sun 11:00
                EntryDateTime = (new DateTime(2017, 5, 22))
                    .AddHours(this.nightFromHigh),
                ExitDateTime = (new DateTime(2017, 5, 22))
                    .AddHours(this.nightTo).AddMinutes(-1)
            };
            var factory = new FeeCalculatorFactory(this.constantRepository);
            var instance = factory.NewFeeCalculator(parkingRecord);

            Assert.IsType<NightFeeCalculator>(instance);
        }

        [Fact]
        public void NewFeeCalculator_ReturnsEarlyBird_WhenBorder1() {
            var parkingRecord = new ParkingRecord() {
                EntryDateTime = (new DateTime(2017, 5, 15))
                                .AddHours(this.ebFromLow),
                ExitDateTime = (new DateTime(2017, 5, 15))
                                .AddHours(this.ebToHigh)
            };
            var factory = new FeeCalculatorFactory(this.constantRepository);
            var instance = factory.NewFeeCalculator(parkingRecord);

            Assert.IsType<EarlyBirdFeeCalculator>(instance);
        }

        [Fact]
        public void NewFeeCalculator_ReturnsEarlyBird_WhenBorder2() {
            var parkingRecord = new ParkingRecord() {
                EntryDateTime = (new DateTime(2017, 5, 15))
                                .AddHours(this.ebFromHigh),
                ExitDateTime = (new DateTime(2017, 5, 15))
                                .AddHours(this.ebToLow)
            };
            var factory = new FeeCalculatorFactory(this.constantRepository);
            var instance = factory.NewFeeCalculator(parkingRecord);

            Assert.IsType<EarlyBirdFeeCalculator>(instance);
        }

    }
}
