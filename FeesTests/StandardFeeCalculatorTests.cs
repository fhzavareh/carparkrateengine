using System;
using CarPark.Data;
using CarPark.Fees;
using Xunit;
// ReSharper disable PossibleInvalidOperationException

namespace CarPark.Tests.FeesTests {
    public class StandardFeeCalculatorTests {
        private readonly ConstantRepository constantRepository;

        // Better to repeat these here in the test than make them public in 
        // the class
        private const string HourlyRateKey =
            "CarPark.Fees.StandardFeeCalculator.HourlyRate";
        private const string MaxDailyRateKey =
            "CarPark.Fees.StandardFeeCalculator.MaxDailyRate";
        private const string MaxDailyHoursKey = // max hrs before max rate used
            "CarPark.Fees.StandardFeeCalculator.MaxDailyHours";

        private readonly double hourlyRate;
        private readonly double maxDailyRate;
        private readonly int maxDailyHours;


        public StandardFeeCalculatorTests() {
            this.constantRepository = new HardCodedConstantRepository();
            // Bad practice to perform potentially I/O operation
            // in the constructor -- but this is a just a test class!
            this.hourlyRate = this.constantRepository
                .TryGetValue<double?>(HourlyRateKey).Value;
            this.maxDailyRate = this.constantRepository.TryGetValue<double?>(
                MaxDailyRateKey).Value;
            this.maxDailyHours = this.constantRepository.TryGetValue<int?>(
                MaxDailyHoursKey).Value;
        }

        [Fact]
        public void Fee_ReturnsMax_WhenSingleDayLongerThanMax() {
            var entry = DateTime.Now.Date;
            var exit = entry.AddHours(this.maxDailyHours).AddMinutes(1);
            var calculator = new StandardFeeCalculator(entry, exit,
                this.constantRepository);
            double expectedFee = this.maxDailyRate;
            double fair = calculator.Fee();

            Assert.Equal(expectedFee, fair);
        }

        [Fact]
        public void Fee_ReturnsCorrectVal_WhenMultipleDays() {
            var entry = DateTime.Now.Date.AddHours(22);
            var exit = DateTime.Now.Date.AddDays(2).AddHours(2);

            var calculator = new StandardFeeCalculator(entry, exit,
                this.constantRepository);
            // 2 hrs, 1 day, 2 hours altogether
            double expectedFee = 4 * this.hourlyRate + this.maxDailyRate;
            double fair = calculator.Fee();

            Assert.Equal(expectedFee, fair);
        }

        [Fact]
        public void Fee_ReturnsCorrectVal_WhenSingleDayLessThanMax_NoneBorder() {
            int hours = this.maxDailyHours - 2;
            var entry = DateTime.Now.Date.AddHours(2);
            var exit = entry.AddHours(hours).AddMinutes(-10);

            // we expect rounding up
            var calculator = new StandardFeeCalculator(entry, exit,
                this.constantRepository);
            double expectedFee = hours * this.hourlyRate;
            double fair = calculator.Fee();

            Assert.Equal(expectedFee, fair);
        }

        // Test when entry at 12:00 AM
        [Fact]
        public void Fee_ReturnsCorrectVal_WhenSingleDayLessThanMax_Border1() {
            var entry = DateTime.Now.Date;
            var exit = entry.AddHours(this.maxDailyHours);
            var calculator = new StandardFeeCalculator(entry, exit,
                this.constantRepository);
            double expectedFee = this.maxDailyHours * this.hourlyRate;
            double fair = calculator.Fee();

            Assert.Equal(expectedFee, fair);
        }

        [Fact]
        public void Fee_ReturnsCorrectVal_WhenSingleDayLessThanMax_Border2() {
            var entry = DateTime.Now.Date.AddHours(-this.maxDailyHours);
            var exit = DateTime.Now.Date;
            var calculator = new StandardFeeCalculator(entry, exit,
                this.constantRepository);
            double expectedFee = this.maxDailyHours * this.hourlyRate;
            double fair = calculator.Fee();

            Assert.Equal(expectedFee, fair);
        }

        // I've moved this to constructor
        //[Fact]
        //public void Fee_Fails_WhenEntryAfterExit()
        //{
        //    var entry = DateTime.Now;
        //    var exit = entry.AddHours(-1);
        //    var calculator = new StandardFeeCalculator(entry, exit,
        //        this.constantRepository);

        //    Assert.Throws<InvalidOperationException>(
        //        () => calculator.Fee());
        //}

    }
}
