﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarPark.Data {
    public class PropertyNotFoundException : Exception {
        public PropertyNotFoundException(string key, Type type) {
            this.Message = $"Could not find a property with key {key} " +
                           $"and type {type.FullName}";
        }

        public override string Message { get; }
    }
}
