﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace CarPark.Data {
    public abstract class ConstantRepository {
        // A dictionary to keep track of whether a type is nullable or not.
        // In .NET Core, calling IsClass involves reflection, which is expensive
        // Note that a small set of types are likely to be checked many times
        private static readonly IDictionary<Type, bool>
            NullableCheckDictionary = new Dictionary<Type, bool>();

        protected bool TypeIsNullable(Type type) {
            // TryGetValue is faster than calling Contains first
            bool exists = NullableCheckDictionary.TryGetValue(
                type, out bool isNullable);

            if (!exists) {
                // First check sees if we have Nullable<T>, e.g., int?
                isNullable = Nullable.GetUnderlyingType(type) != null
                    || type.GetTypeInfo().IsClass;
                NullableCheckDictionary.Add(type, isNullable);
            }

            return isNullable;
        }

        /// <summary>
        /// Gets a T property's value.
        /// </summary>
        /// <param name="key">Property key.</param>
        /// <returns>
        /// Property's value, or T's default value if no property with this
        /// key exists, or if the key exists, but the value is
        /// not a valid T type.
        /// </returns>
        /// <remarks>
        /// The default value of a type is not necessarily null (except for 
        /// reference types). e.g., non-nullable numeric value have a default 
        /// value of 0, which makes it impossible to tell whether the key does
        /// not exists or the actual value of the property is 0. For this 
        /// specific example, a better approach would be to call this method 
        /// using the nullable version of the numeric value type (e.g., int?).
        /// </remarks>
        public abstract TValue GetValue<TValue>(string key);

        /// <summary>
        /// Gets a T property's value.
        /// </summary>
        /// <typeparam name="TValue">
        /// A reference type or a nullable value type. A non-nullable type
        /// will result in a runtime error.
        /// </typeparam>
        /// <param name="key">Property key.</param>
        /// <returns>Property's value.</returns>
        /// <exception cref="PropertyNotFoundException">Thrown when a property 
        /// with this key and type does not exist.</exception>
        public virtual TValue TryGetValue<TValue>(string key) {
            if (!this.TypeIsNullable(typeof(TValue))) {
                throw new InvalidOperationException("TValue must be nullable.");
            }

            var value = this.GetValue<TValue>(key);

            if (value == null) {
                throw new PropertyNotFoundException(key, typeof(TValue));
            }
            return value;
        }

    }
}
