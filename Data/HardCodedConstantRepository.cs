﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarPark.Data {
    // Method documentation in the base class
    public class HardCodedConstantRepository : ConstantRepository {
        private static readonly Dictionary<string, object> Properties
            = new Dictionary<string, object>()
            {
                {"CarPark.Fees.StandardFeeCalculator.HourlyRate", 5D },
                {"CarPark.Fees.StandardFeeCalculator.MaxDailyRate", 20D },
                {"CarPark.Fees.StandardFeeCalculator.MaxDailyHours", 3 },

                {"CarPark.Fees.NightFeeCalculator.FlatRate", 6.5 },
                {"CarPark.Fees.EarlyBirdFeeCalculator.FlatRate", 13D },
                {"CarPark.Fees.WeekendFeeCalculator.FlatRate", 10D },

                {"CarPark.Fees.FeeCalculatorFactory.EarlyBirdFromLow", 6D },
                {"CarPark.Fees.FeeCalculatorFactory.EarlyBirdFromHigh", 9D },
                {"CarPark.Fees.FeeCalculatorFactory.EarlyBirdToLow", 15.5 },
                {"CarPark.Fees.FeeCalculatorFactory.EarlyBirdToHigh", 23.5 },
                {"CarPark.Fees.FeeCalculatorFactory.NightFromLow", 18D },
                {"CarPark.Fees.FeeCalculatorFactory.NightFromHigh", 24D },
                {"CarPark.Fees.FeeCalculatorFactory.NightTo", 30D }
            };

        public override TValue GetValue<TValue>(string key) {
            Properties.TryGetValue(key, out object value);

            if (value is TValue) {
                return (TValue)value;
            }

            return default(TValue);
        }
    }
}
