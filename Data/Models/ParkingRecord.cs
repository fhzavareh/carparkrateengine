﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CarPark.Data.Models {
    public class ParkingRecord {
        [Key]
        public int ParkingInstanceId { get; set; }
       
        // Virtual to enable lazy-loading, if supported by the ORM
        public virtual Vehicle Vehicle { get; set; }

        [Required]
        public DateTime EntryDateTime { get; set; }
        
        // This is not required for obvious reasons
        public DateTime ExitDateTime { get; set; }
    }
}
