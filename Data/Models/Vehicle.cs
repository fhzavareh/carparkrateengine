﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CarPark.Data.Models {
    public class Vehicle
    {
        [Key]
        [RegularExpression("^[a-zA-Z0-9]{1,20}$")]
        public string RegistrationNumber { get; set; }

        [Required]
        [RegularExpression("^[a-zA-Z0-9]{1,20}$")]
        public string Make { get; set; }

        [Required]
        [RegularExpression("^[a-zA-Z0-9]{1,20}$")]
        public string Model { get; set; }

        [Required]
        // We can't use current year as it is not a constant
        // so we're settling for just a minimum check
        [Range(1900, int.MaxValue)]
        public int Year { get; set; }
    }
}
