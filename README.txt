Car Park Rate Calculation Engine

- Usage -

Run ConsoleInterface project and follow the instructions.

Note: The projects were developed using VS 2017 and target .NET Core 1.1.

- Description -
The Car Park Rate Calculation Engine is a simple program that calculates a 
parking fee based on an entry date and time and an exit date and time. The 
purpose of this program is to demonstrate the usage of several principles and 
best practices, including TDD and writing effective unit tests, various design 
patterns, dependency inversion principle, dependency injection (DI) and 
others.

The current version supports the following special rates and falls back to 
Standard Rate in all other cases.

	* Early Bird: 	$13 Flat Rate
			Enter between 6:00 AM and 9:00 AM
			Exit between 3:30 PM and 11:30 PM
	* Night Rate:	$6.50 Flat Rate
			Enter between 6:00 PM to midnight (weekdays)
			Exit before 6 AM the following day
	* Weekend Rate	$10 Flat Rate
			Enter any time past midnight Friday to Sunday
			Exit any time before midnight Sunday
	* Standard Rate	$5 per hour, max $20 per calendar day
	
- Design Notes -

-- Fee calculation algorithm --
A simple method to implement this would have been to use the Strategy Pattern,
creating an IFeeCalculator interface and a concrete implementation to take care
of everything. This would be a good design if the current algorithm were not to
be extended in the future.
The current design allows extending the algorithm without leading to a large
single-class implementation with low cohesion. e.g., adding a more elaborate
algorithm for a certain time of year that charges different rates for different
times of a day.
Some classes are small, and very similar (e.g., WeekendFeeCalculator, 
NightFeeCalculator). That is to support extending these rate types in the
future, if necessary.

-- Data layer --
The data layer assumes that a persistence mechanism will be used to store the
constants for production.

Repository Pattern is used to support this, as well as to encapsulate the ORM 
and decouple the rest of the application from its details. The Pattern also
makes unit testing much easier.

Model classes use Data Annotations to make user input validation easier for 
ASP.NET MVC (including Web APIs). Additionally, they will be used for Entity 
Framework code-first approach to define DB fields. I'd like to keep DB-specific 
details and constraints outside the models and use Fluent API to define them.

-- Tests --
StandardFeeCalculator and FeeCalculatorFactory were developed using TDD. 
Ideally, other fee calculators would have unit tests as well.

-- Other remarks --
All classes follow the Explicit Dependencies Principle and receive their 
dependencies in their constructor (that is why FeeCalculatorFactory is not 
static).
Ideally, dependency injection (DI) will be used. ASP.NET MVC Core supports DI
natively. So, for example, we could have a ConstantRepository implementation
that communicates with a DB, but use the hard-coded version that we have at the 
moment for unit testing (which is the way it should be -- unit tests shouldn't 
be testing the DB repository -- except for the repository's unit tests).