﻿using System;
using System.Collections.Generic;
using System.Text;
using CarPark.Data;
using CarPark.Data.Models;

namespace CarPark.Fees {
    public class FeeCalculatorFactory {

        #region Constants
        private const string EbFromLowKey =
            "CarPark.Fees.FeeCalculatorFactory.EarlyBirdFromLow";
        private const string EbFromHighKey =
            "CarPark.Fees.FeeCalculatorFactory.EarlyBirdFromHigh";
        private const string EbToLowKey =
            "CarPark.Fees.FeeCalculatorFactory.EarlyBirdToLow";
        private const string EbToHighKey =
            "CarPark.Fees.FeeCalculatorFactory.EarlyBirdToHigh";
        private const string NightFromLowKey =
            "CarPark.Fees.FeeCalculatorFactory.NightFromLow";
        private const string NightFromHighKey =
            "CarPark.Fees.FeeCalculatorFactory.NightFromHigh";
        private const string NigthToKey =
            "CarPark.Fees.FeeCalculatorFactory.NightTo";

        // NOTE: TryGetValue never returns null -- either a value or 
        // PropertyNotFoundException. However, if we do not pass nullable,
        // it returns default value (e.g., 0) instead of exception when
        // not found.
        // We are retrieving the constants through these fields to avoid
        // getting default values when key does not exist. Default value of
        // numeric value types is zero and won't be caught.
        // Also note these are potentially I/O ops, so not suitable for 
        // initializing in constructor

        private double? ebFromLow;
        private double EarlyBirdFromLow {
            get {
                if (this.ebFromLow == null) {
                    this.ebFromLow =
                        this.constantRepository.TryGetValue<double?>(EbFromLowKey);
                }
                return this.ebFromLow.Value;
            }
        }
        private double? ebFromHigh;
        private double EarlyBirdFromHigh {
            get {
                if (this.ebFromHigh == null) {
                    this.ebFromHigh =
                        this.constantRepository.TryGetValue<double?>(EbFromHighKey);
                }
                return this.ebFromHigh.Value;
            }
        }
        private double? ebToLow;
        private double EarlyBirdToLow {
            get {
                if (this.ebToLow == null) {
                    this.ebToLow =
                        this.constantRepository.TryGetValue<double?>(EbToLowKey);
                }
                return this.ebToLow.Value;
            }
        }
        private double? ebToHigh;
        private double EarlyBirdToHigh {
            get {
                if (this.ebToHigh == null) {
                    this.ebToHigh =
                        this.constantRepository.TryGetValue<double?>(EbToHighKey);
                }
                return this.ebToHigh.Value;
            }
        }
        private double? nightFromLow;
        private double NightFromLow {
            get {
                if (this.nightFromLow == null) {
                    this.nightFromLow =
                        this.constantRepository.TryGetValue<double?>(NightFromLowKey);
                }
                return this.nightFromLow.Value;
            }
        }
        private double? nightFromHigh;
        private double NightFromHigh {
            get {
                if (this.nightFromHigh == null) {
                    this.nightFromHigh =
                        this.constantRepository.TryGetValue<double?>(NightFromHighKey);
                }
                return this.nightFromHigh.Value;
            }
        }

        private double? nightTo;
        private double NightTo {
            get {
                if (this.nightTo == null) {
                    this.nightTo =
                        this.constantRepository.TryGetValue<double?>(NigthToKey);
                }
                return this.nightTo.Value;
            }
        }
        #endregion

        private readonly ConstantRepository constantRepository;

        public FeeCalculatorFactory(ConstantRepository constantRepository) {
            this.constantRepository = constantRepository;
        }

        public virtual FeeCalculator NewFeeCalculator(
            ParkingRecord parkingRecord) {

            var entry = parkingRecord.EntryDateTime;
            var exit = parkingRecord.ExitDateTime;

            // Input validation
            if (entry == null)
                throw new ArgumentNullException("Vehicle EntryDateTime " +
                                          "cannot be null.");
            if (exit == null)
                throw new ArgumentNullException("Vehicle ExitDateTime " +
                                            "cannot be null.");
            if (exit < entry)
                throw new ArgumentException("Exit DateTime cannot be after " +
                                            "entry DateTime.");

            // Return a special fee calculator if applicable
            if (this.IsEarlyBirdRate(entry, exit))
                return new EarlyBirdFeeCalculator(this.constantRepository);
            if (this.IsNightRate(entry, exit))
                return new NightFeeCalculator(this.constantRepository);
            if (this.IsWeekendRate(entry, exit))
                return new WeekendFeeCalculator(this.constantRepository);

            // Return the standard fee calculator
            return new StandardFeeCalculator(entry, exit,
                this.constantRepository);
        }

        protected bool IsEarlyBirdRate(DateTime entry, DateTime exit) {
            var rawDate = entry.Date;
            return
                !this.IsWeekend(entry.DayOfWeek) // is week day
                && entry >= rawDate.AddHours(this.EarlyBirdFromLow)
                && entry <= rawDate.AddHours(this.EarlyBirdFromHigh)
                && exit >= rawDate.AddHours(this.EarlyBirdToLow)
                && exit <= rawDate.AddHours(this.EarlyBirdToHigh);
        }

        protected bool IsNightRate(DateTime entry, DateTime exit) {
            // Be careful with midnight entry! Where our constraint says
            // "midnight Friday", it means Saturday 00:00
            DateTime rawDate;
            if (entry == entry.Date)
                rawDate = entry.AddMilliseconds(-1).Date;
            else
                rawDate = entry.Date;

            return
                !this.IsWeekend(entry.DayOfWeek)
                && entry >= rawDate.AddHours(this.NightFromLow)
                && entry <= rawDate.AddHours(this.NightFromHigh)
                && exit < rawDate.AddHours(this.NightTo); // exclusive
        }

        protected bool IsWeekendRate(DateTime entry, DateTime exit) {
            var rawDate = entry.Date;
            // Following Monday 00:00
            var followingMonday = this.GetNext(rawDate, DayOfWeek.Monday);
            return
                this.IsWeekend(entry.DayOfWeek) // enter Sat or Sun
                && exit < followingMonday; // exclusive
        }

        protected bool IsWeekend(DayOfWeek day) {
            return day == DayOfWeek.Saturday || day == DayOfWeek.Sunday;
        }

        /// <summary>
        /// Gets the next date with a certain DayOfWeek.
        /// </summary>
        /// <param name="date"></param>
        /// <param name="nextDayOfWeek"></param>
        /// <returns>The next date with DayeOfWeek of <code>nextDayOfWeek</code> with
        /// the same time component as <code>date</code>.</returns>
        protected DateTime GetNext(DateTime date, DayOfWeek nextDayOfWeek) {
            var thisDayOfWeek = date.DayOfWeek;
            int diff = thisDayOfWeek - nextDayOfWeek;
            return date.Date.AddDays(((6 - diff) % 7) + 1);
        }
    }
}
