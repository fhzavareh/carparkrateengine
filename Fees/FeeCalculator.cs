﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace CarPark.Fees {
    public abstract class FeeCalculator {

        protected FeeCalculator() {
            this.CanonicalName = FeeCalculator.CreateCanonicalName(this);
            if (string.IsNullOrEmpty(this.CanonicalName)) {
                throw new NotSupportedException("Cannot create a canonical " +
                    "name for this class as it does not follow the " +
                    "standard name format. Use FeeCalculator(string) " +
                    "instead.");
            }
        }

        protected FeeCalculator(string canonicalName) {
            this.CanonicalName = canonicalName;
        }

        public string CanonicalName { get; }

        public abstract double Fee();

        // Static mainly because it is used in the constructor and
        // using instance methods in the constructor is bad practice
        private static string CreateCanonicalName(FeeCalculator instance) {
            const string regex = "^([A-Z]{1}[a-z]*)*FeeCalculator$";

            string typeName = instance.GetType().Name;
            var match = Regex.Match(typeName, regex);

            if (!match.Success) return string.Empty;

            string canonicalName = "";
            var captures = match.Groups[1].Captures;
            // Note that because this loop is unlikely to run
            // more than 3 times, StringBuilder is not any faster,
            // if not slower
            for (int i = 0; i < captures.Count; i++) {
                canonicalName += captures[i].Value + " ";
            }
            return canonicalName.Trim();
        }
    }
}
