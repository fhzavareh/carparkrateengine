﻿using System;
using System.Collections.Generic;
using System.Text;
using CarPark.Data;

namespace CarPark.Fees {
    public class NightFeeCalculator : FeeCalculator {

        private const string RateKey =
            "CarPark.Fees.NightFeeCalculator.FlatRate";

        private double? flatRate;
        private double FlatRate {
            get {
                if (this.flatRate == null) {
                    this.flatRate =
                        this.constantRepository.TryGetValue<double?>(RateKey);
                }
                return this.flatRate.Value;
            }
        }

        private readonly ConstantRepository constantRepository;

        // Instances must be created only through FeeCalculatorFactory
        public NightFeeCalculator(ConstantRepository constantRepository) {
            this.constantRepository = constantRepository;
        }

        public override double Fee() {
            return this.FlatRate;
        }
    }
}


