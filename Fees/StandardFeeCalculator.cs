﻿using System;
using System.Collections.Generic;
using System.Text;
using CarPark.Data;
using System.Diagnostics.Contracts;
using System.Reflection.PortableExecutable;
// ReSharper disable PossibleInvalidOperationException

namespace CarPark.Fees {
    public class StandardFeeCalculator : FeeCalculator {

        #region Constants
        private const string HourlyRateKey =
            "CarPark.Fees.StandardFeeCalculator.HourlyRate";
        private const string MaxDailyRateKey =
             "CarPark.Fees.StandardFeeCalculator.MaxDailyRate";
        private const string MaxDailyHoursKey = // max hrs before max rate used
            "CarPark.Fees.StandardFeeCalculator.MaxDailyHours";

        // NOTE: TryGetValue never returns null -- either a value or 
        // PropertyNotFoundException. However, if we do not pass nullable,
        // it returns default value (e.g., 0) instead of exception when
        // not found.
        // We are retrieving the constants through these fields to avoid
        // getting default values when key does not exist. Default value of
        // numeric value types is zero and won't be caught.
        // Also note these are potentially I/O ops, so not suitable for 
        // initializing in constructor

        private double? hourlyRate;
        private double HourlyRate {
            get {
                if (this.hourlyRate == null) {
                    this.hourlyRate =
                        this.constantRepository.TryGetValue<double?>(HourlyRateKey);
                }
                return this.hourlyRate.Value;
            }
        }

        private double? maxDailyRate;
        private double MaxDailyRate {
            get {
                if (this.maxDailyRate == null) {
                    this.maxDailyRate =
                        this.constantRepository.TryGetValue<double?>(MaxDailyRateKey);
                }
                return this.maxDailyRate.Value;
            }
        }

        private int? maxDailyHours;
        private int MaxDailyHours {
            get {
                if (this.maxDailyHours == null) {
                    this.maxDailyHours =
                        this.constantRepository.TryGetValue<int?>(MaxDailyHoursKey);
                }
                return this.maxDailyHours.Value;
            }
        }
        #endregion

        private readonly DateTime entry;
        private readonly DateTime exit;
        private readonly ConstantRepository constantRepository;

        private double? fair;

        // Instances must be created only through FeeCalculatorFactory
        public StandardFeeCalculator(DateTime entry, DateTime exit,
            ConstantRepository constantRepository) {
            if (entry == null)
                throw new ArgumentNullException(nameof(entry));
            if (exit == null)
                throw new ArgumentNullException(nameof(exit));
            if (constantRepository == null)
                throw new ArgumentNullException(nameof(constantRepository));

            // Remove seconds and milliseconds from DateTimes
            this.entry = entry
                .AddSeconds(-entry.Second)
                .AddMilliseconds(-entry.Millisecond);
            this.exit = exit
                .AddSeconds(-entry.Second)
                .AddMilliseconds(-entry.Millisecond);

            if (this.exit < this.entry) {
                throw new ArgumentException("Exit cannot be before " +
                                                    "entry.");
            }

            this.constantRepository = constantRepository;
        }

        public override double Fee() {
            if (this.fair != null) return this.fair.Value;

            double firstDayFee, fullDaysFee = 0, lastDayFee;
            int firstDayHours;
            // Calculate number of calendar days
            int days = this.exit.Date.Subtract(this.entry.Date).Days;

            if (days == 0) { // exit same day
                firstDayHours = this.GetHours(
                    this.entry, this.exit
                );
                return this.CalculateFeeForHours(firstDayHours);
            }

            // Calculate first day's fee
            firstDayHours = this.GetHours(
               this.entry, this.entry.AddDays(1).Date
               );
            firstDayFee = this.CalculateFeeForHours(firstDayHours);

            // Calculate for full days
            if (days > 1) {
                fullDaysFee = (days - 1) * this.CalculateFeeForHours(24);
            }

            // Calculate for last day
            int lastDayHours = this.GetHours(
                this.exit.Date, this.exit);
            lastDayFee = this.CalculateFeeForHours(lastDayHours);

            this.fair = firstDayFee + fullDaysFee + lastDayFee;

            return this.fair.Value;
        }

        private double CalculateFeeForHours(int hours) {
            return hours > this.MaxDailyHours
                ? this.MaxDailyRate
                : hours * this.HourlyRate;
        }

        /// <summary>
        /// Returns the number of hours between to DateTimes, rounded to the 
        /// smallest bigger integral value (i.e. ceiling). For example, 
        /// 2 hours and 1 minute is rounded to 3 hours.
        /// </summary>
        /// <param name="from">From DateTime.</param>
        /// <param name="to">To DateTime.</param>
        /// <returns>Number of hours.</returns>
        private int GetHours(DateTime from, DateTime to) {
            return Convert.ToInt32(
                Math.Ceiling(to.Subtract(from).TotalMinutes / 60D)
                );
        }

        private double CalculateSingleDayFee(DateTime entry, DateTime exit) {
            if (entry.Date != exit.Date) {
                throw new ArgumentException("Entry and exit must be in the " +
                                            "same day.");
            }
            var timeSpan = exit.Subtract(entry);
            int hours = timeSpan.Hours;
            if (timeSpan.Minutes > 0) hours += 1;

            if (hours > this.maxDailyHours) return this.MaxDailyRate;
            // else
            return hours * this.HourlyRate;
        }
    }
}
