﻿using System;
using System.Collections.Generic;
using System.Text;
using CarPark.Data;

namespace CarPark.Fees {
    public class WeekendFeeCalculator : FeeCalculator {

        private const string RateKey =
            "CarPark.Fees.WeekendFeeCalculator.FlatRate";
        private double? flatRate;
        private double FlatRate {
            get {
                if (this.flatRate == null) {
                    this.flatRate =
                        this.constantRepository.TryGetValue<double?>(RateKey);
                }
                return this.flatRate.Value;
            }
        }

        private readonly ConstantRepository constantRepository;

        // Instances must be created only through FeeCalculatorFactory
        public WeekendFeeCalculator(ConstantRepository constantRepository) {
            this.constantRepository = constantRepository;
        }

        public override double Fee() {
            return this.FlatRate;
        }
    }
}
